@echo off
title PG-COS
goto :BIOS

:BIOS
color 17
@cls
echo BIOS Boot menu v1.0.0
echo 1. Regular boot
echo 2. Secure boot
echo 3. Network boot
set /p c=
if /I "%c%" EQU "1" goto START
if /I "%c%" EQU "2" goto STARTsec
if /I "%c%" EQU "3" goto STARTnet
goto :BIOS

:STARTsec
@cls
echo Booting in secure mode from CD..
ping localhost -n 15 >NUL
goto :defcolor

:STARTnet
@cls
echo Booting in Network mode from CD..
ping localhost -n 20 >NUL
goto :defcolor


:START
set dire=A
color 07
echo Booting From CD...
set networkname=blank
ping localhost -n 5 >NUL
@cls
echo Copyright 2019 PixelGame LLC.
goto :HOME

:defcolor
color 07
goto :HOME

:BSOD
@cls
color 17
echo An error has occured, your pc will reboot and try again..
TIMEOUT 5 >NUL
goto :START

:HOME
set /p c=%dire%:\
if /I "%c%" EQU "help" goto :help
if /I "%c%" EQU "clear" goto :clearpane
if /I "%c%" EQU "network.setup" goto :networksetup
if /I "%c%" EQU "device.stat" goto :devicestat
if /I "%c%" EQU "storage" goto :storage
if /I "%c%" EQU "DEL" goto :DEL
if /I "%c%" EQU "rsrt" goto :START
if /I "%c%" EQU "INST" goto :INST
if /I "%c%" EQU "DIRCHANGE" goto :DIRCHANGE
if /I "%c%" EQU "RAND" goto :RAND
if /I "%c%" EQU "DIRSYS" goto :DIRSYS
if /I "%c%" EQU "CONFIG" goto :CONFIG
if /I "%c%" EQU "COLOR" goto :COLOR
if /I "%c%" EQU "ECHO" goto :ECHO
if /I "%c%" EQU "secret.COS" goto :secret
if /I "%c%" EQU "BSOD.frc" goto :BSOD
goto :ERORR

:COLOR
echo Change the color of the display,
echo The first digit is for the background,
echo the second digit is for the text.
echo Type in the digits, and press enter!
echo 0	=	Black	 	8	=	Gray
echo 1	=	Blue	 	9	=	Light Blue
echo 2	=	Green	 	A	=	Light Green
echo 3	=	Aqua	 	B	=	Light Aqua
echo 4	=	Red	 	    C	=	Light Red
echo 5	=	Purple	 	D	=	Light Purple
echo 6	=	Yellow	 	E	=	Light Yellow
echo 7	=	White	 	F	=	Bright White
set /p color=
color %color%
goto :HOME


:CONFIG
echo Select the file you want to configure:
echo 1. COSINSTALL.CINST
echo 2. FLPdriver.CINST
set /p c=
if /I "%c%" EQU "1" goto :installREG
if /I "%c%" EQU "2" goto :installREG
if /I "%c%" EQU "BACK" goto :HOME
goto :CONFIG

:installREG
echo Installing...
ping localhost -n 15 >NUL
echo Installed!
goto :HOME

:ECHO
echo What would you like to echo?
set /p echo=
echo %echo%
goto :HOME


:DIRSYS
echo Systems Files on Disk
echo 1. COMMANDS.COSCOM
echo 2. DATA42.COS
echo 3. DIR.DIRFC
echo 4. (HIDDEN)
goto :HOME

:RAND
SET /A RAND=%RANDOM%%%1000+1
echo %RAND%
goto :HOME

:secret
color 02
echo %random% %random% %random% %random% %random% %random% %random% %random% %random%
set loop=0
echo %random%
set /a loop=%loop%+1 
if "%loop%"=="100" goto :HOME
goto :secret



:INST
echo Creating Save file...
echo Type username:
set /p %user%=
xcopy C:\Users\%user%\Desktop\windos\COS\PG-COS\Storage\88j.txt
goto :HOME


:DIRCHANGE
echo Type the directory SL (e.g. change A:\ to Z:\)
set /p dire=
goto :HOME


:ERORR
echo.
echo "%c%" is not a command.
goto :HOME

:help
echo -COS Commands-
echo Help - Displays Command Screen
echo network.setup - Allows you to connect to a nearby network
echo device.stat - Shows you your device status and info
echo clear - Clears command pane
echo storage - Shows Storage data
echo DEL - Delete files on Disk
echo rsrt - Restarts the computer
echo INST - Allows you to create a save file
echo DIRCHANGE - Change the Directory for the System to operate in
echo RAND - Generates a random number between 1 and 1000
echo DIRSYS - Shows files in the System Folder
echo CONFIG - Configure or Install a file
echo COLOR - Allows you to change the color of the display
echo ECHO - Allows you to have the system echo a peice of text
echo BSOD.frc - Does a BSOD on purpose and restarts your PC
pause >NUL
goto :HOME

:networksetup
echo.
echo Type the network name:
set /p networkname=
echo Type the network Password:
set /p networkpass=
echo Connected!
goto :HOME

:clearpane
@cls
goto :HOME

:devicestat
echo Device Name: COS-PC
echo Total Storage: 10 KB
echo COS Version: v1.0.0
goto :HOME

:storage
echo Total Storage 10 KB
set cnt=0
for %%A in (*) do set /a cnt+=1
echo %cnt%/10 files on Disk
if /I "%cnt%" EQU "10" goto :FULL
goto :HOME

:FULL
echo Disk Full! Type "DEL" to delete some files!
goto :HOME

:DEL
echo Type the name of the file you want to delete!
set /p del=
echo Type username:
set /p user=
DEL C:\Users\%user%\Desktop\windos\COS\PG-COS\Storage\%del%
if "%del%" EQU "PG-OS.bat" goto :ERRDEL
goto :HOME

:ERRDEL
echo You can not delete "%del%"! It is a critical System File!
goto :HOME